using CryptoCSharp.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace CryptoCSharp.Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void shouldProduceTheGoodBase64()
        {
            var logger = Mock.Of<ILogger<QualifyingSet>>();
            QualifyingSet qualifyingSet = new QualifyingSet(logger);

            string expectedAnswer = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
            string actualAnswer = qualifyingSet.HexString2B64String("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d");

            Assert.AreEqual(expectedAnswer, actualAnswer);
        }

        [Test]
        public void shouldReturnAnEmptyString()
        {
            var logger = Mock.Of<ILogger<QualifyingSet>>();
            QualifyingSet qualifyingSet = new QualifyingSet(logger);

            string expectedAnswer = "";
            string actualAnswer = qualifyingSet.HexString2B64String("");

            Assert.AreEqual(expectedAnswer, actualAnswer);
        }
    }
}