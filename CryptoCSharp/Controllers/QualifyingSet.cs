﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CryptoCSharp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class QualifyingSet : ControllerBase
    {
        private readonly ILogger<QualifyingSet> _logger;

        public QualifyingSet(ILogger<QualifyingSet> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string HexString2B64String(string input)
        {
            return System.Convert.ToBase64String(this.HexStringToHex(input));
        }

        [HttpGet]
        public byte[] HexStringToHex(string inputHex)
        {
            var resultantArray = new byte[inputHex.Length / 2];
            for (var i = 0; i < resultantArray.Length; i++)
            {
                resultantArray[i] = System.Convert.ToByte(inputHex.Substring(i * 2, 2), 16);
            }
            return resultantArray;
        }


    }
}
